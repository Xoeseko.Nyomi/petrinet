<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PetriNet Structure Reference</title>
    <link rel="stylesheet" type="text/css" href="../css/jazzy.css" />
    <link rel="stylesheet" type="text/css" href="../css/highlight.css" />
    <meta charset='utf-8'>
    <script src="../js/jquery.min.js" defer></script>
    <script src="../js/jazzy.js" defer></script>
    
  </head>
  <body>
    <a name="//apple_ref/swift/Struct/PetriNet" class="dashAnchor"></a>
    <a title="PetriNet Structure Reference"></a>
    <header>
      <div class="content-wrapper">
        <p><a href="../index.html"> Docs</a> (96% documented)</p>
      </div>
    </header>
    <div class="content-wrapper">
      <p id="breadcrumbs">
        <a href="../index.html"> Reference</a>
        <img id="carat" src="../img/carat.png" />
        PetriNet Structure Reference
      </p>
    </div>
    <div class="content-wrapper">
      <nav class="sidebar">
        <ul class="nav-groups">
          <li class="nav-group-name">
            <a href="../Protocols.html">Protocols</a>
            <ul class="nav-group-tasks">
              <li class="nav-group-task">
                <a href="../Protocols/Place.html">Place</a>
              </li>
              <li class="nav-group-task">
                <a href="../Protocols.html#/s:8PetriNet10TransitionP">Transition</a>
              </li>
            </ul>
          </li>
          <li class="nav-group-name">
            <a href="../Structs.html">Structures</a>
            <ul class="nav-group-tasks">
              <li class="nav-group-task">
                <a href="../Structs/Marking.html">Marking</a>
              </li>
              <li class="nav-group-task">
                <a href="../Structs/PetriNet.html">PetriNet</a>
              </li>
              <li class="nav-group-task">
                <a href="../Structs/PetriNet/ArcDescription.html">– ArcDescription</a>
              </li>
              <li class="nav-group-task">
                <a href="../Structs/TotalMap.html">TotalMap</a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <article class="main-content">
        <section>
          <section class="section">
            <h1>PetriNet</h1>
              <div class="declaration">
                <div class="language">
                  <pre class="highlight swift"><code><span class="kd">public</span> <span class="kd">struct</span> <span class="kt">PetriNet</span><span class="o">&lt;</span><span class="kt">PlaceType</span><span class="p">,</span> <span class="kt">TransitionType</span><span class="o">&gt;</span>
  <span class="k">where</span> <span class="kt">PlaceType</span><span class="p">:</span> <span class="kt"><a href="../Protocols/Place.html">Place</a></span><span class="p">,</span> <span class="kt">PlaceType</span><span class="o">.</span><span class="kt">Content</span> <span class="o">==</span> <span class="kt">Int</span><span class="p">,</span> <span class="kt">TransitionType</span><span class="p">:</span> <span class="kt"><a href="../Protocols.html#/s:8PetriNet10TransitionP">Transition</a></span></code></pre>

                </div>
              </div>
            <p>A Petri net.</p>

<p><code>PetriNet</code> is a generic type, accepting two types representing the set of places and the set
of transitions that structurally compose the model. Both should conform to <code>CaseIterable</code>,
which guarantees that the set of places (resp. transitions) is bounded, and known statically.
The following example illustrates how to declare the places and transition of a simple Petri
net representing an on/off switch:</p>
<pre class="highlight swift"><code><span class="kd">enum</span> <span class="kt">P</span><span class="p">:</span> <span class="kt">Place</span> <span class="p">{</span>
  <span class="kd">typealias</span> <span class="kt">Content</span> <span class="o">=</span> <span class="kt">Int</span>
  <span class="k">case</span> <span class="n">on</span><span class="p">,</span> <span class="n">off</span>
<span class="p">}</span>

<span class="kd">enum</span> <span class="kt">T</span><span class="p">:</span> <span class="kt">Transition</span> <span class="p">{</span>
  <span class="k">case</span> <span class="n">switchOn</span><span class="p">,</span> <span class="n">switchOff</span>
<span class="p">}</span>
</code></pre>

<p>Petri net instances are created by providing the list of the preconditions and postconditions
that compose them. These should be provided in the form of arc descriptions (i.e. instances of
<code>ArcDescription</code>) and fed directly to the Petri net&rsquo;s initializer. The following example shows
how to create an instance of the on/off switch:</p>
<pre class="highlight swift"><code><span class="k">let</span> <span class="nv">model</span> <span class="o">=</span> <span class="kt">PetriNet</span><span class="o">&lt;</span><span class="kt">P</span><span class="p">,</span> <span class="kt">T</span><span class="o">&gt;</span><span class="p">(</span>
  <span class="o">.</span><span class="nf">pre</span><span class="p">(</span><span class="nv">from</span><span class="p">:</span> <span class="o">.</span><span class="n">on</span><span class="p">,</span> <span class="nv">to</span><span class="p">:</span> <span class="o">.</span><span class="n">switchOff</span><span class="p">),</span>
  <span class="o">.</span><span class="nf">post</span><span class="p">(</span><span class="nv">from</span><span class="p">:</span> <span class="o">.</span><span class="n">switchOff</span><span class="p">,</span> <span class="nv">to</span><span class="p">:</span> <span class="o">.</span><span class="n">off</span><span class="p">),</span>
  <span class="o">.</span><span class="nf">pre</span><span class="p">(</span><span class="nv">from</span><span class="p">:</span> <span class="o">.</span><span class="n">off</span><span class="p">,</span> <span class="nv">to</span><span class="p">:</span> <span class="o">.</span><span class="n">switchOn</span><span class="p">),</span>
  <span class="o">.</span><span class="nf">post</span><span class="p">(</span><span class="nv">from</span><span class="p">:</span> <span class="o">.</span><span class="n">switchOn</span><span class="p">,</span> <span class="nv">to</span><span class="p">:</span> <span class="o">.</span><span class="n">on</span><span class="p">),</span>
<span class="p">)</span>
</code></pre>

<p>Petri net instances only represent the structual part of the corresponding model, meaning that
markings should be stored externally. They can however be used to compute the marking resulting
from the firing of a particular transition, using the method <code>fire(transition:from:)</code>. The
following example illustrates this method&rsquo;s usage:</p>
<pre class="highlight swift"><code><span class="k">if</span> <span class="k">let</span> <span class="nv">marking</span> <span class="o">=</span> <span class="n">model</span><span class="o">.</span><span class="nf">fire</span><span class="p">(</span><span class="o">.</span><span class="n">switchOn</span><span class="p">,</span> <span class="nv">from</span><span class="p">:</span> <span class="p">[</span><span class="o">.</span><span class="nv">on</span><span class="p">:</span> <span class="mi">0</span><span class="p">,</span> <span class="o">.</span><span class="nv">off</span><span class="p">:</span> <span class="mi">1</span><span class="p">])</span> <span class="p">{</span>
  <span class="nf">print</span><span class="p">(</span><span class="n">marking</span><span class="p">)</span>
<span class="p">}</span>
<span class="c1">// Prints "[.on: 1, .off: 0]"</span>
</code></pre>

          </section>
          <section class="section task-group-section">
            <div class="task-group">
              <ul>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAV8ArcLabela"></a>
                    <a name="//apple_ref/swift/Alias/ArcLabel" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAV8ArcLabela">ArcLabel</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Undocumented</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="kd">typealias</span> <span class="kt">ArcLabel</span> <span class="o">=</span> <span class="kt">Int</span></code></pre>

                        </div>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAV14ArcDescriptionV"></a>
                    <a name="//apple_ref/swift/Struct/ArcDescription" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAV14ArcDescriptionV">ArcDescription</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>The description of an arc.</p>

                        <a href="../Structs/PetriNet/ArcDescription.html" class="slightly-smaller">See more</a>
                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="kd">struct</span> <span class="kt">ArcDescription</span></code></pre>

                        </div>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAV5inputSDyq_SDyxSiGGvp"></a>
                    <a name="//apple_ref/swift/Property/input" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAV5inputSDyq_SDyxSiGGvp">input</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>This net&rsquo;s input matrix.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="k">let</span> <span class="nv">input</span><span class="p">:</span> <span class="p">[</span><span class="kt">TransitionType</span> <span class="p">:</span> <span class="p">[</span><span class="kt">PlaceType</span> <span class="p">:</span> <span class="kt"><a href="../Structs/PetriNet.html#/s:8PetriNetAAV8ArcLabela">ArcLabel</a></span><span class="p">]]</span></code></pre>

                        </div>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAV6outputSDyq_SDyxSiGGvp"></a>
                    <a name="//apple_ref/swift/Property/output" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAV6outputSDyq_SDyxSiGGvp">output</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>This net&rsquo;s output matrix.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="k">let</span> <span class="nv">output</span><span class="p">:</span> <span class="p">[</span><span class="kt">TransitionType</span> <span class="p">:</span> <span class="p">[</span><span class="kt">PlaceType</span> <span class="p">:</span> <span class="kt"><a href="../Structs/PetriNet.html#/s:8PetriNetAAV8ArcLabela">ArcLabel</a></span><span class="p">]]</span></code></pre>

                        </div>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAVyAByxq_Gqd__cSTRd__AB14ArcDescriptionVyxq__G7ElementRtd__lufc"></a>
                    <a name="//apple_ref/swift/Method/init(_:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAVyAByxq_Gqd__cSTRd__AB14ArcDescriptionVyxq__G7ElementRtd__lufc">init(_:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Initializes a Petri net with a sequence describing its preconditions and postconditions.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="kd">init</span><span class="o">&lt;</span><span class="kt">Arcs</span><span class="o">&gt;</span><span class="p">(</span><span class="n">_</span> <span class="nv">arcs</span><span class="p">:</span> <span class="kt">Arcs</span><span class="p">)</span> <span class="k">where</span> <span class="kt">Arcs</span> <span class="p">:</span> <span class="kt">Sequence</span><span class="p">,</span> <span class="kt">Arcs</span><span class="o">.</span><span class="kt">Element</span> <span class="o">==</span> <span class="kt">PetriNet</span><span class="o">&lt;</span><span class="kt">PlaceType</span><span class="p">,</span> <span class="kt">TransitionType</span><span class="o">&gt;.</span><span class="kt"><a href="../Structs/PetriNet/ArcDescription.html">ArcDescription</a></span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>arcs</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>A sequence containing the descriptions of the Petri net&rsquo;s arcs.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAVyAByxq_GAB14ArcDescriptionVyxq__Gd_tcfc"></a>
                    <a name="//apple_ref/swift/Method/init(_:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAVyAByxq_GAB14ArcDescriptionVyxq__Gd_tcfc">init(_:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Initializes a Petri net with descriptions of its preconditions and postconditions.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="nf">init</span><span class="p">(</span><span class="n">_</span> <span class="nv">arcs</span><span class="p">:</span> <span class="kt"><a href="../Structs/PetriNet/ArcDescription.html">ArcDescription</a></span><span class="o">...</span><span class="p">)</span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>arcs</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>A variadic argument representing the descriptions of the Petri net&rsquo;s arcs.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </section>
                  </div>
                </li>
                <li class="item">
                  <div>
                    <code>
                    <a name="/s:8PetriNetAAV4fire10transition4fromAA7MarkingVyxGSgq__AHtF"></a>
                    <a name="//apple_ref/swift/Method/fire(transition:from:)" class="dashAnchor"></a>
                    <a class="token" href="#/s:8PetriNetAAV4fire10transition4fromAA7MarkingVyxGSgq__AHtF">fire(transition:from:)</a>
                    </code>
                  </div>
                  <div class="height-container">
                    <div class="pointer-container"></div>
                    <section class="section">
                      <div class="pointer"></div>
                      <div class="abstract">
                        <p>Computes the marking resulting from the firing of the given transition, from the given
marking, assuming the former is fireable.</p>

                      </div>
                      <div class="declaration">
                        <h4>Declaration</h4>
                        <div class="language">
                          <p class="aside-title">Swift</p>
                          <pre class="highlight swift"><code><span class="kd">public</span> <span class="kd">func</span> <span class="nf">fire</span><span class="p">(</span><span class="nv">transition</span><span class="p">:</span> <span class="kt">TransitionType</span><span class="p">,</span> <span class="n">from</span> <span class="nv">marking</span><span class="p">:</span> <span class="kt"><a href="../Structs/Marking.html">Marking</a></span><span class="o">&lt;</span><span class="kt">PlaceType</span><span class="o">&gt;</span><span class="p">)</span>
  <span class="o">-&gt;</span> <span class="kt"><a href="../Structs/Marking.html">Marking</a></span><span class="o">&lt;</span><span class="kt">PlaceType</span><span class="o">&gt;</span><span class="p">?</span></code></pre>

                        </div>
                      </div>
                      <div>
                        <h4>Parameters</h4>
                        <table class="graybox">
                          <tbody>
                            <tr>
                              <td>
                                <code>
                                <em>transition</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The transition to fire.</p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <code>
                                <em>marking</em>
                                </code>
                              </td>
                              <td>
                                <div>
                                  <p>The marking from which the given transition should be fired.</p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div>
                        <h4>Return Value</h4>
                        <p>The marking that results from the firing of the given transition if it is fireable, or
<code>nil</code> otherwise.</p>
                      </div>
                    </section>
                  </div>
                </li>
              </ul>
            </div>
          </section>
        </section>
        <section id="footer">
          <p>&copy; 2019 <a class="link" href="" target="_blank" rel="external"></a>. All rights reserved. (Last updated: 2019-10-01)</p>
          <p>Generated by <a class="link" href="https://github.com/realm/jazzy" target="_blank" rel="external">jazzy ♪♫ v0.11.2</a>, a <a class="link" href="https://realm.io" target="_blank" rel="external">Realm</a> project.</p>
        </section>
      </article>
    </div>
  </body>
</div>
</html>
