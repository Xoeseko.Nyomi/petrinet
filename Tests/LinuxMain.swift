import XCTest

import PetriNetTests

var tests = [XCTestCaseEntry]()
tests += PetriNetTests.allTests()
XCTMain(tests)
